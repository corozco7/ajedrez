package Logica;

public class Pieza {
    
    private int fila;
    private int columna;
    private Jugador jugador;
    private tipoPieza tipo;
    private String imagen;
    
    public enum Jugador {
        BLANCAS,
        NEGRAS
    }

    public enum tipoPieza {
        REY,
        DAMA,
        ALFIL,
        TORRE,
        CABALLO,
        PEON
    }

    public Pieza(int fila, int columna, Jugador jugador, tipoPieza tipo, String imagen) {
        this.fila = fila;
        this.columna = columna;
        this.jugador = jugador;
        this.tipo = tipo;
        this.imagen = imagen;
    }

    public int getFila() {
        return fila;
    }

    public int getColumna() {
        return columna;
    }

    public tipoPieza getTipo() {
        return tipo;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public String getImagen() {
        return imagen;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }
    
}
