package Presentacion;

import Logica.Pieza;

public interface Interfaz {
    
    Pieza posicionPieza(int columna, int fila);
    void moverPieza(int columnaInicial, int filaInicial,
            int columnaFinal, int filaFinal);
    
}
