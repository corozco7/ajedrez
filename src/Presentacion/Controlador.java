package Presentacion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class Controlador implements ActionListener, MouseListener,
        MouseMotionListener, WindowListener {

    private Vista miVentana;
    private final int puerto = 50000;
    private int filaPieza;
    private int columnaPieza;
    private ServerSocket listener;
    private PrintWriter out;
    private Socket socket;

    public Controlador(Vista athis) {
        miVentana = athis;
    }

    public PrintWriter getPrintWriter() {
        return out;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        columnaPieza = (e.getPoint().x - miVentana.getModelo().getPosX())
                / miVentana.getModelo().getTamañoCasilla();
        filaPieza = 7 - (e.getPoint().y - miVentana.getModelo().getPosY())
                / miVentana.getModelo().getTamañoCasilla();
        miVentana.getModelo().setPiezaMovida(miVentana.getModelo()
                .getInterfazAjedrez().posicionPieza(filaPieza, columnaPieza));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        int columnaActual = (e.getPoint().x - miVentana.getModelo().getPosX())
                / miVentana.getModelo().getTamañoCasilla();
        int filaActual = 7 - (e.getPoint().y - miVentana.getModelo().getPosY())
                / miVentana.getModelo().getTamañoCasilla();

        miVentana.getModelo().getInterfazAjedrez().moverPieza(
                filaPieza, columnaPieza,
                filaActual, columnaActual
        );
        miVentana.getModelo().setPiezaMovida(null);
        miVentana.getModelo().setPosicionPiezaMovida(null);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        miVentana.getModelo().setPosicionPiezaMovida(e.getPoint());
        miVentana.repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    private void runSocketServer() {
        Executors.newFixedThreadPool(1).execute(new Runnable() {
            @Override
            public void run() {
                try {
                    listener = new ServerSocket(puerto);
                    System.out.println("Escuchando por el puerto " + puerto);
                    socket = listener.accept();
                    out = new PrintWriter(socket.getOutputStream(), true);
                    var in = new Scanner(socket.getInputStream());
                    recibirMovimiento(in);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void runSocketClient() {
        try {
            socket = new Socket("localhost", puerto);
            System.out.println("cliente conectado por el puerto " + puerto);
            var in = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream(), true);
            Executors.newFixedThreadPool(1).execute(new Runnable() {
                @Override
                public void run() {
                    recibirMovimiento(in);
                }

            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == miVentana.getResetBtn()) {
            miVentana.getModelo().reiniciar();
            miVentana.repaint();
            try {
                if(listener != null){
                    listener.close();
                }
                if (socket != null) {
                    socket.close();
                }
                miVentana.getClientBtn().setEnabled(true);
                miVentana.getServerBtn().setEnabled(true);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if (e.getSource() == miVentana.getClientBtn()) {
            miVentana.getClientBtn().setEnabled(false);
            miVentana.getServerBtn().setEnabled(false);
            miVentana.setTitle("Ajedrez Cliente");
            runSocketClient();
            JOptionPane.showMessageDialog(miVentana, "Conectado "
                    + "al puerto " + puerto);
        }
        if (e.getSource() == miVentana.getServerBtn()) {
            miVentana.getServerBtn().setEnabled(false);
            miVentana.getClientBtn().setEnabled(false);
            miVentana.setTitle("Ajedrez Servidor");
            runSocketServer();
            JOptionPane.showMessageDialog(miVentana, "Escuchando "
                    + "por el puerto " + puerto);
        }
    }

    private void recibirMovimiento(Scanner in) {
        while (in.hasNextLine()) {
            var moveStr = in.nextLine();
            System.out.println("pieza movida recibida: " + moveStr);
            var arrStr = moveStr.split(",");
            var colInicial = Integer.parseInt(arrStr[0]);
            var filInicial = Integer.parseInt(arrStr[1]);
            var colFinal = Integer.parseInt(arrStr[2]);
            var filFinal = Integer.parseInt(arrStr[3]);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    miVentana.getModelo().moverPieza(colInicial,
                            filInicial, colFinal,
                            filFinal);
                    miVentana.repaint();
                }
            });
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        if (out != null) {
            out.close();
        }
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }
}
