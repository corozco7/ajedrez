package Presentacion;

import Logica.Pieza;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.imageio.ImageIO;

public class Modelo {

    private Vista miVentana;
    private final int alturaTablero = 600;
    private final int anchuraTablero = 600;
    private final int posX = 60;
    private final int posY = 60;
    private final int tamañoCasilla = 60;
    private Map<String, Image> imagenPiezas = new HashMap<>();
    private Set<Pieza> piezas = new HashSet<>();
    private Interfaz interfazAjedrez;
    private Pieza piezaMovida;
    private Point posicionPiezaMovida;
    private Pieza.Jugador turno = Pieza.Jugador.BLANCAS;

    public Vista getMiVentana() {
        if (miVentana == null) {
            miVentana = new Vista(this);
        }
        return miVentana;
    }

    public Modelo() {
        String[] nombrePiezas = {
            "bb",
            "bk",
            "bn",
            "bp",
            "bq",
            "br",
            "wb",
            "wk",
            "wn",
            "wp",
            "wq",
            "wr",};
        try {
            for (String np : nombrePiezas) {
                Image imagen = cargarImagen(np + ".png");
                imagenPiezas.put(np, imagen);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getMiVentana().addMouseListener(getMiVentana().getControl());
        getMiVentana().addMouseMotionListener(getMiVentana().getControl());
        getMiVentana().addWindowListener(getMiVentana().getControl());
    }

    public void iniciar() {
        getMiVentana().setSize(alturaTablero, anchuraTablero);
        getMiVentana().setVisible(true);
    }

    public void generarTablero(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                generarCasillas(g2, 2 * i, 2 * j, true);
                generarCasillas(g2, 1 + 2 * i, 1 + 2 * j, true);

                generarCasillas(g2, 1 + 2 * i, 2 * j, false);
                generarCasillas(g2, 2 * i, 1 + 2 * j, false);
            }
        }

        generarPiezas(g2);
    }

    private void generarCasillas(Graphics2D g2, int fila, int columna, boolean blanco) {
        g2.setColor(blanco ? Color.white : Color.gray);
        g2.fillRect(posX + fila * tamañoCasilla,
                posY + columna * tamañoCasilla,
                tamañoCasilla,
                tamañoCasilla
        );
    }

    private Image cargarImagen(String nombreImagen) throws Exception {
        ClassLoader carga = getClass().getClassLoader();
        URL res = carga.getResource("Presentacion/Piezas/" + nombreImagen);
        if (res == null) {
            return null;
        } else {
            File archivo = new File(res.toURI());
            return ImageIO.read(archivo);
        }
    }

    private void añadirImagen(Graphics2D g2, int fila, int columna, String pieza) {
        Image img = imagenPiezas.get(pieza);
        g2.drawImage(
                img,
                posX + columna * tamañoCasilla,
                posY + (7 - fila) * tamañoCasilla,
                tamañoCasilla, tamañoCasilla, null
        );
    }

    private void generarPiezas(Graphics2D g2) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Pieza p = interfazAjedrez.posicionPieza(j, i);
                if (p != null && p != piezaMovida) {
                    añadirImagen(g2, j, i, p.getImagen());
                }
            }
        }

        if (piezaMovida != null && posicionPiezaMovida != null) {
            Image img = imagenPiezas.get(piezaMovida.getImagen());
            g2.drawImage(img,
                    posicionPiezaMovida.x - tamañoCasilla / 2,
                    posicionPiezaMovida.y - tamañoCasilla / 2,
                    tamañoCasilla, tamañoCasilla, null);
        }
    }

    public void reiniciar() {
        piezas.removeAll(piezas);
        for (int i = 0; i < 2; i++) {
            piezas.add(new Pieza(0 + i * 7, 7, Pieza.Jugador.NEGRAS,
                    Pieza.tipoPieza.TORRE, "br"));
            piezas.add(new Pieza(0 + i * 7, 0, Pieza.Jugador.BLANCAS,
                    Pieza.tipoPieza.TORRE, "wr"));

            piezas.add(new Pieza(1 + i * 5, 7, Pieza.Jugador.NEGRAS,
                    Pieza.tipoPieza.CABALLO, "bn"));
            piezas.add(new Pieza(1 + i * 5, 0, Pieza.Jugador.BLANCAS,
                    Pieza.tipoPieza.CABALLO, "wn"));

            piezas.add(new Pieza(2 + i * 3, 7, Pieza.Jugador.NEGRAS,
                    Pieza.tipoPieza.ALFIL, "bb"));
            piezas.add(new Pieza(2 + i * 3, 0, Pieza.Jugador.BLANCAS,
                    Pieza.tipoPieza.ALFIL, "wb"));
        }

        for (int i = 0; i < 8; i++) {
            piezas.add(new Pieza(i, 6, Pieza.Jugador.NEGRAS,
                    Pieza.tipoPieza.PEON, "bp"));
            piezas.add(new Pieza(i, 1, Pieza.Jugador.BLANCAS,
                    Pieza.tipoPieza.PEON, "wp"));
        }

        piezas.add(new Pieza(3, 7, Pieza.Jugador.NEGRAS,
                Pieza.tipoPieza.DAMA, "bq"));
        piezas.add(new Pieza(3, 0, Pieza.Jugador.BLANCAS,
                Pieza.tipoPieza.DAMA, "wq"));
        piezas.add(new Pieza(4, 7, Pieza.Jugador.NEGRAS,
                Pieza.tipoPieza.REY, "bk"));
        piezas.add(new Pieza(4, 0, Pieza.Jugador.BLANCAS,
                Pieza.tipoPieza.REY, "wk"));

        turno = Pieza.Jugador.BLANCAS;
    }

    public Pieza posicionPieza(int columna, int fila) {
        for (Pieza p : piezas) {
            if (p.getColumna() == columna && p.getFila() == fila) {
                return p;
            }
        }
        return null;
    }

    /*@Override
    public String toString() {
        String desc = "";
        for (int i = 7; i >= 0; i--) {
            desc += "" + i;
            for (int j = 0; j < 8; j++) {
                Pieza p = posicionPieza(i, j);
                if(p == null){
                    desc += " .";
                } else {
                    switch(p.getTipo()){
                        case REY -> desc += p.getJugador() == Pieza.Jugador.BLANCAS ?
                                    " k":" K";
                        case DAMA -> desc += p.getJugador() == Pieza.Jugador.BLANCAS ?
                                    " q":" Q";
                        case ALFIL -> desc += p.getJugador() == Pieza.Jugador.BLANCAS ?
                                    " b":" B";
                        case TORRE -> desc += p.getJugador() == Pieza.Jugador.BLANCAS ?
                                    " r":" R";
                        case CABALLO -> desc += p.getJugador() == Pieza.Jugador.BLANCAS ?
                                    " n":" N";
                        case PEON -> desc += p.getJugador() == Pieza.Jugador.BLANCAS ?
                                    " p":" P";
                    }
                }
            }
            desc += "\n";
        }
        desc += "  0 1 2 3 4 5 6 7";
        return desc;
    }*/
    public void moverPieza(int columnaInicial, int filaInicial,
            int columnaFinal, int filaFinal) {

        Pieza candidata = posicionPieza(columnaInicial, filaInicial);
        if (candidata == null || candidata.getJugador() != turno
                || columnaInicial == columnaFinal && filaInicial == filaFinal) {
            return;
        }

        Pieza objetivo = posicionPieza(columnaFinal, filaFinal);
        if (objetivo != null) {
            if (objetivo.getJugador() == candidata.getJugador()) {
                return;
            } else {
                piezas.remove(objetivo);
            }
        }

        candidata.setColumna(columnaFinal);
        candidata.setFila(filaFinal);
        turno = turno == Pieza.Jugador.BLANCAS ? Pieza.Jugador.NEGRAS
                : Pieza.Jugador.BLANCAS;
    }

    public void setInterfazAjedrez(Interfaz interfazAjedrez) {
        this.interfazAjedrez = interfazAjedrez;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public int getTamañoCasilla() {
        return tamañoCasilla;
    }

    public Interfaz getInterfazAjedrez() {
        return interfazAjedrez;
    }

    public Pieza getPiezaMovida() {
        return piezaMovida;
    }

    public Point getPosicionPiezaMovida() {
        return posicionPiezaMovida;
    }

    public void setPiezaMovida(Pieza piezaMovida) {
        this.piezaMovida = piezaMovida;
    }

    public void setPosicionPiezaMovida(Point posicionPiezaMovida) {
        this.posicionPiezaMovida = posicionPiezaMovida;
    }

}
