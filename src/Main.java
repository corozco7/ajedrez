
import Presentacion.Modelo;

public class Main {
    
    private Modelo Ajedrez;
    
    public Main(){
        Ajedrez = new Modelo();
        Ajedrez.iniciar();
    }
    
    public static void main(String[] args) {
        new Main();
    }
    
}
